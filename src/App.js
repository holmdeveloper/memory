
import { useEffect, useState } from 'react';
import './App.css';
import SingelCard from './componets/SingelCard';
const cardImg = [
  { "src": "/img/femal-1.png", matched: false },
  { "src": "/img/ghoul-1.png" , matched: false},
  { "src": "/img/lie-1.png" , matched: false},
  { "src": "/img/house-1.png" , matched: false},
  { "src": "/img/lie2-1.png" , matched: false},
  { "src": "/img/femal1-1.png", matched: false },
  { "src": "/img/ghoul2-1.png", matched: false },
  { "src": "/img/wolf-1.png", matched: false },
  { "src": "/img/zombie-1.png", matched: false },
  { "src": "/img/mumier-1.png", matched: false },

]



function App() {
  const [cards, setCards] = useState([])
  const [turns, setTurns] = useState(0)
  const [choiseOne, setChoiseOne] = useState(null)
  const [choiseTwo, setChoiseTwo] = useState(null)
  const [disabled, setDisabled] = useState(false)
  const shuffleCard = () => {
    const shuffledCard = [...cardImg, ...cardImg]
      .sort(() => Math.random() - 0.5)
      .map((card) => ({ ...card, id: Math.random() }))
      
      setChoiseOne(null)
      setChoiseTwo(null)
    setCards(shuffledCard)
    setTurns(0)
  }
  const handelChoice = (card) => {
    choiseOne ? setChoiseTwo(card) : setChoiseOne(card)
  }
  useEffect(() => {
    
    if (choiseOne && choiseTwo) {
      setDisabled(true)
      if (choiseOne.src === choiseTwo.src) {
      setCards(prevCards =>{
        return prevCards.map(card =>{
          if(card.src === choiseOne.src){
            return{...card, matched: true}
          } else{
            return card
            
          }
        })
      })
      
        resetTurn()
      } else{
      
      setTimeout(()=> resetTurn(),1000)
      }
      
    }
  }, [choiseOne, choiseTwo])
  let highScore = localStorage.getItem('highScore');
  if (highScore !== null) {
    highScore = parseInt(highScore);
  } else {
    highScore = 0;
  }
  if (turns > highScore) {
    highScore = turns;
    localStorage.setItem('highScore', highScore);
  }

  const resetTurn = () => {
    setChoiseOne(null)
    setChoiseTwo(null)
    setTurns(prevTurns => prevTurns + 1)
    setDisabled(false)
  }
  useEffect(()=>{
 
    shuffleCard()
    
  },[])

  return (
    <div className="App">
      <h1>Ghoul Game</h1>
      <div><button onClick={shuffleCard}>Start game</button></div><div className="score"> Your best score: <button>{highScore}</button></div> 
      <div className='card-grid'>
     
        {cards.map(card => (
          <SingelCard
            key={card.id}
            card={card}
            handelChoice={handelChoice}
            flipped={card === choiseOne || card === choiseTwo || card.matched}
            disabled={disabled}
            />
        ))}

      </div>
     
      <p>Turns: {turns}</p>
    </div>
  );
}

export default App;
