import "./SingelCard.css"

export default function SingelCard({card, handelChoice, flipped, disabled}) {
    const handelClick =()=>{
        if(!disabled){
            handelChoice(card)
        }
    }
    return (
        <div className="card" key={card.id}>
            <div className={flipped? "flipped" :""}>
                <img className="front" src={card.src} alt="card front" />
                <img className="back" src="/img/cover.png" alt="card back"  onClick={handelClick}/>
            </div>
        </div>
    )
}